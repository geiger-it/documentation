# Vocabulary

Ubiquitous Language https://thedomaindrivendesign.io/developing-the-ubiquitous-language/

**Account**:
<u>Application Account</u>: When you register for a website or online service, you create a unique account that segregates your information from other users of that system. Also see *tenant.*
<u>Customer/Business Account</u>: A financial legal entity (person or company). Not all customers and organizations will have an account with our business. But to be able to place an order, we require our customers to have an account with us. The financial team will establish credit limits.

**Branch:**
A term in Acumatica (ERP) for financial separation between a parent company and a subsidiary. See "Company" and "Business Unit"

**Business Unit (BU):**
The operational divisions of the Geiger companies. Corporate Programs is a department or division of Geiger. Bank Express is a division of Crestline.
*See "Company" for the legal divisions of Geiger.*

**Company:**
The legal and financial divisions of Geiger. 
*See "Business Unit" for the operational divisions of Geiger.*

**Customer:**
A person who has or intends to place an order with our company. Each business will have a different customer record for the same person.

**Department:**
*See "Business Unit"*

**Organization:** A company, division, franchise, department, or any abstract grouping of people. An organization can contain one or more children and only one parent (a tree data structure). Some applications may wish to make a distinction between organizations and teams.  An application's team of users (e.g. order verifiers) may work within one or more or across organizations, but are grouped to serve an application-specific function, such as a role or permission.

**Prospect:**
A person who is a potential customer.

**Sales Associate:**
An employee of a business unit that works with customers to place orders. An associate can work on any order within the business (TBD: whether they can manage orders for the whole company, tenant, or just the business unit). An associate does not get commission for orders unless they associate is also a sales rep. 
*See "Sales Rep"*

**Sales Support:**
Some Sales Partners have employees who perform tasks on their behalf. These associates should be able to perform most of the tasks a Sales Partner would when it comes to working with customers and fulfilling orders. 
The OMS Sales Support role: Sales Support associates should not have access to overall financials, commissions, reports, etc. However, a user could have both a sales support and a report viewer role.

**Sales Partner:**
A person who takes commission from affiliated orders. A partner may be independent or an associate of our company.

**Software as a Service (SaaS):**
A system design pattern that treats software as a product that can be delivered to multiple 

**Supplier:** A vendor who provides Geiger with products, decoration services, or both.  

**Tenant:**
A system design where a single application instance is shared by multiple user (or user group) accounts in a way that offers some or complete isolation of data from other accounts.

**Vendor:** A company that Geiger does business with, whether it's a supplier for goods and services, a development partner, or any other 3rd party service.